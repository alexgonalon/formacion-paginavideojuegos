import { Detalle } from './pages/Detalle';
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Home } from './pages/Home';
import { Cabecera } from './componentes/cabecera/Cabecera';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Cabecera />
          <Home />
        </Route>
        <Route exact path="/detalle">
          <Detalle />
        </Route>
      </Switch>
    </Router>
  );
}
export default App;
