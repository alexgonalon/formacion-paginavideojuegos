import { Principal } from '../componentes/principal/Principal';
import './Detalle.css';
import '../index.css';

export const Detalle = () => {
  return (
    <Principal>
      <div className="cabecera_detalle">
        <img
          className="background-image"
          src="./img/gow_background.png"
          alt="portada fondo de God of War"></img>
        <div class="cabecera_detalle_info">
          <h1 className="titulo-videojuego">God of war</h1>
          <div className="contenedor">
            <span className="precio">19.99€</span>
            <div className="button install-game">
              <span className="texto-botones">INSTALL GAME</span>
            </div>
          </div>
        </div>
      </div>

      <span className="articulo">
        <img
          src="./img/img_detailgow.png"
          srcset="./img/img_detailgow@2x.png 2x,
          ./img/img_detailgow@3x.png 3x"
          className="img-detailgow"
          alt="kratos"></img>
        <span className="texto">
          Kratos, que ha dejado atrás el mundo de los dioses, deberá adaptarse a
          tierras que no le son familiares, afrontar peligros inesperados y
          aprovechar una segunda oportunidad de ejercer como padre. Junto a su
          hijo Atreus se aventurará en lo más profundo e inclemente de las
          tierras de Midgard y luchará por culminar una búsqueda hondamente
          personal. • Viaja a un mundo oscuro y elemental de temibles criaturas
          directamente extraído de la mitología nórdica. • Vence a tus enemigos
          en encarnizados combates cuerpo a cuerpo donde el pensamiento táctico
          y la precisión letal te darán la ventaja. • Descubre una historia
          apasionante y emocional en la que Kratos se verá obligado a controlar
          la ira que tanto lo ha caracterizado.
        </span>
      </span>
    </Principal>
  );
};
