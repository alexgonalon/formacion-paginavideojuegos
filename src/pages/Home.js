import { useHistory } from 'react-router';
import { Card } from '../componentes/card/Card';
import data from '../data/videojuegos.json';
import { Principal } from '../componentes/principal/Principal';

export function Home() {
  let videojuegos = data;
  let history = useHistory();

  function handleClick() {
    history.push('/detalle');
  }

  return (
    <Principal>
      {videojuegos.games.map(game => (
        <Card
          key={game.id}
          imgSrc={game.imgSrc}
          title={game.title}
          price={game.price}
          onClick={handleClick}
        />
      ))}
    </Principal>
  );
}
