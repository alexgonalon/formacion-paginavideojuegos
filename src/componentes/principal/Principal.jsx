import './Principal.css';
export const Principal = props => {
  return <div className="principal">{props.children}</div>;
};
