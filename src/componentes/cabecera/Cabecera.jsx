import './Cabecera.css';

export const Cabecera = props => {
  return (
    <div className="cabecera">
      <img src="./img/league_of_legends_background.png"></img>

      <div className="cabecera_interior_info">
        <h1 className="titulo_cabecera">League of legends</h1>
        <span className="lol_texto">
          League of Legends is a fast-paced, competitive online game that blends
          <br />
          the speed and intensity of an RTS with RPG elements.
        </span>
        <div className="button-group">
          <div className="button install-now" onClick={props.onClick}>
            <span className="texto-install">INSTALL GAME</span>
          </div>

          <div className="button favorites" onClick={props.onClick}>
            <span className="texto-favorites">ADD TO FAVORITES</span>
          </div>
        </div>
      </div>
    </div>
  );
};
