import './Card.css';

export const Card = props => {
  return (
    <div className="card">
      <img src={props.imgSrc} alt={`Portada ${props.title}`}></img>

      <div className="footer">
        <div className="game-info">
          <span>{props.title}</span>
          <span>{props.price}</span>
        </div>

        <div className="button buy-now" onClick={props.onClick}>
          <span className="texto-botones">BUY NOW</span>
        </div>
      </div>
    </div>
  );
};
